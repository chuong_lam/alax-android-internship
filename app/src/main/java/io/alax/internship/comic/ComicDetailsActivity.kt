package io.alax.internship.comic

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_comic_details.*

class ComicDetailsActivity : AppCompatActivity() {
  private lateinit var service: ComicService

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_comic_details)

    val bundle = intent.getBundleExtra("comic")
    val name = bundle?.getString("name")
    val category = bundle?.getInt("category")?:0
    val image = bundle?.getString("image")

    txtName.text = name
    Picasso.with(applicationContext).load(image).error(R.drawable.error).into(imgComic)

    service = ApiUtils.getComicService()

    loadCategoryId(category)

  }

  private fun loadCategoryId(categoryId: Int) {
    service.getCategoryId(categoryId).subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe {
          txtCate.text = it.title
        }
  }

}