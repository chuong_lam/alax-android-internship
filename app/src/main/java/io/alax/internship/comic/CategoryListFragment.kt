package io.alax.internship.comic

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CategoryListFragment(private val categoryId: Int) : Fragment() {
  private lateinit var rootView: View
  private lateinit var viewAdapter: ComicAdapter
  private lateinit var viewManager: RecyclerView.LayoutManager
  private lateinit var recyclerView: RecyclerView
  private lateinit var service: ComicService
  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    rootView = inflater.inflate(R.layout.activity_category, container, false)

    service = ApiUtils.getComicService()
    
    initView()

    loadComic()

    return rootView
  }

  private fun loadComic() {
    service.getComic(categoryId).subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe {
          viewAdapter.updateData(it)
          viewAdapter.notifyDataSetChanged()
        }
  }

  private fun initView() {
    viewManager = LinearLayoutManager(context)
    viewAdapter = ComicAdapter(context!!, arrayListOf())
    recyclerView = rootView.findViewById<RecyclerView>(R.id.rv_comic).apply {
      setHasFixedSize(true)
      layoutManager = viewManager
      adapter = viewAdapter
    }
  }
}

