package io.alax.internship.comic

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {
  private lateinit var service: ComicService
  private lateinit var viewPager: ViewPager
  private lateinit var layout: TabLayout
  private lateinit var adapter: CategoryAdapter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    service = ApiUtils.getComicService()

    loadCategory()
  }

  private fun initView(listCategories: List<Category>) {
    adapter = CategoryAdapter(supportFragmentManager, listCategories)
    viewPager = findViewById(R.id.vp_category)
    viewPager.adapter = adapter
    layout = this.findViewById(R.id.tl_category)
    layout.setupWithViewPager(viewPager)
  }

  private fun loadCategory() {
    service.getCategories().subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnError {

        }
        .doOnComplete {

        }
        .subscribe {
          initView(it)
        }
  }
}
