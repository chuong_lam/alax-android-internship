package io.alax.internship.comic

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ComicService {
  @GET("categories")
  fun getCategories(): Observable<List<Category>>

  @GET("comics")
  fun getComic(@Query("categoryId") categoryId: Int): Observable<List<Comic>>

  @GET("categories/{categoryId}")
  fun getCategoryId(@Path("categoryId") id: Int): Observable<Category>
}