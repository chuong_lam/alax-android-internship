package io.alax.internship.comic

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class ComicDetailsAdapter(private val detailsList: List<Comic>) :
    RecyclerView.Adapter<ComicDetailsAdapter.MyViewHolder>() {
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
    val root = LayoutInflater.from(parent.context)
        .inflate(R.layout.activity_comic_details, parent, false)
    return MyViewHolder(root)
  }

  override fun getItemCount(): Int = detailsList.size

  override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
    val item = detailsList[position]

    holder.nameTit.text = item.name
    holder.category.text = item.categoryId.toString()
    Picasso.with(holder.img.context).load(item.image).error(R.drawable.error).into(holder.img)
  }

  class MyViewHolder(parent: View) : RecyclerView.ViewHolder(parent) {
    var nameTit: TextView = parent.findViewById(R.id.txtName)
    var category: TextView = parent.findViewById(R.id.txtCate)
    var img: ImageView = parent.findViewById(R.id.imgComic)
  }
}