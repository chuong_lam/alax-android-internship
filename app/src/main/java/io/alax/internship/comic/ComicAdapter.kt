package io.alax.internship.comic

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class ComicAdapter(private val context: Context, private val comicList: ArrayList<Comic>) :
    RecyclerView.Adapter<ComicAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val root = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_comic, parent, false)
        return MyViewHolder(root)
    }

    fun updateData(list: List<Comic>) {
        comicList.addAll(list)
    }

    class MyViewHolder(parent: View) : RecyclerView.ViewHolder(parent) {
        var name: TextView = parent.findViewById(R.id.tv_name)
        var type: TextView = parent.findViewById(R.id.tv_chapNum)
        var image: ImageView = parent.findViewById(R.id.image)
    }

    override fun getItemCount() = comicList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = comicList[position]

        holder.name.text = item.name
        holder.type.text = item.chapNum.toString()
        Picasso.with(holder.image.context).load(item.image).error(R.drawable.error).into(holder.image)
        holder.itemView.setOnClickListener { doOpenDetails(item) }
    }

    private fun doOpenDetails(item: Comic) {
        val myIntent = Intent(context, ComicDetailsActivity::class.java)
        val myBundle = Bundle()
        myBundle.putString("name", item.name)
        myBundle.putInt("category", item.categoryId)
        myBundle.putString("image", item.image)
        myIntent.putExtra("comic", myBundle)
        context.startActivity(myIntent)
    }
}