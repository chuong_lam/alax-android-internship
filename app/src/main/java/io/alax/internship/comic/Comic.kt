package io.alax.internship.comic

class Comic(
    val id: Int,
    val name: String,
    val image: String,
    val categoryId: Int,
    val chapNum: Int
) {
}