package io.alax.internship.comic

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class CategoryAdapter(
    fragmentManager: FragmentManager
    , private val categoryList: List<Category>)
  : FragmentStatePagerAdapter(fragmentManager) {
  override fun getItem(position: Int): Fragment {
    return CategoryListFragment(categoryList[position].id)
  }

  override fun getCount(): Int = categoryList.size

  override fun getPageTitle(position: Int): CharSequence? {
    return categoryList[position].title
  }
}