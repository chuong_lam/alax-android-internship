package io.alax.internship.comic

class ApiUtils {
  companion object {
    private const val BASE_URL = "http://10.0.2.2:3000/"
    fun getComicService(): ComicService {
      return RetrofitClient().getClient(BASE_URL)!!.create(ComicService::class.java)
    }
  }
}